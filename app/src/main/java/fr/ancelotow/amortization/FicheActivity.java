package fr.ancelotow.amortization;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.time.format.DateTimeFormatter;

import fr.ancelotow.amortization.entities.Materiel;
import fr.ancelotow.amortization.entities.Session;

public class FicheActivity extends AppCompatActivity {

    TextView tvNom;
    TextView tvValeurEB;
    TextView tvValeurR;
    TextView tvBaseA;
    TextView tvDateAcquis;
    TextView tvDateMES;
    TextView tvDateDebAL;
    TextView tvDateDebAD;
    TextView tvDuree;
    TextView tvTauxL;
    TextView tvCoeff;
    TextView tvTauxD;
    Button btnRetour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiche);
        Materiel materiel = Session.getSession().getMateriel();
        setTitle("Fiche : " + materiel.getNom());
        tvNom = (TextView) findViewById(R.id.tvNom);
        tvValeurEB = (TextView) findViewById(R.id.tvValeurEB);
        tvValeurR = (TextView) findViewById(R.id.tvValeurR);
        tvBaseA = (TextView) findViewById(R.id.tvBaseA);
        tvDateAcquis = (TextView) findViewById(R.id.tvDateAcquis);
        tvDateMES = (TextView) findViewById(R.id.tvDateMES);
        tvDateDebAL = (TextView) findViewById(R.id.tvDateDebAL);
        tvDateDebAD = (TextView) findViewById(R.id.tvDateDebAD);
        tvDuree = (TextView) findViewById(R.id.tvDuree);
        tvTauxL = (TextView) findViewById(R.id.tvTauxL);
        tvCoeff = (TextView) findViewById(R.id.tvCoeff);
        tvTauxD = (TextView) findViewById(R.id.tvTauxD);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        tvNom.setText(materiel.getNom());
        tvValeurEB.setText(Double.toString(materiel.getValeurEB()));
        tvValeurR.setText(Double.toString(materiel.getValeurR()));
        tvBaseA.setText(Double.toString(materiel.getBaseA()));
        DateTimeFormatter formatDate = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            formatDate = DateTimeFormatter.ofPattern("d/MM/uuuu");
            tvDateAcquis.setText(materiel.getDateAcquis().format(formatDate));
            tvDateMES.setText(materiel.getDateMES().format(formatDate));
            tvDateDebAL.setText(materiel.getDateDebAL().format(formatDate));
            tvDateDebAD.setText(materiel.getDateDebAD().format(formatDate));
        }
        tvDuree.setText(Integer.toString(materiel.getDureeReel()));
        tvTauxL.setText(Double.toString(materiel.getTauxL()));
        tvCoeff.setText(Double.toString(materiel.getCoeff()));
        tvTauxD.setText(Double.toString(materiel.getTauxD()));
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(FicheActivity.this,
                        MenuActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        btnRetour.setOnClickListener(retour);
    }
}
