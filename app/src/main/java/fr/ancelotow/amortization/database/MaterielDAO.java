package fr.ancelotow.amortization.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.ancelotow.amortization.entities.Materiel;

public class MaterielDAO{

    private static final String TABLE_NAME = "amortDB";
    public static final String COL_ID_PK = "mat_id";
    public static final String COL_DATECREE = "mat_dateCreer";
    public static final String COL_NOM = "mat_nom";
    public static final String COL_VALEUREB = "mat_valeurEB";
    public static final String COL_VALEURR = "mat_valeurR";
    public static final String COL_DATEACUIS = "mat_dateAcquis";
    public static final String COL_DATEMES = "mat_dateMES";
    public static final String COL_DUREE = "mat_duree";
    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"( \n" +
            COL_ID_PK+" INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            COL_DATECREE+" TEXT, \n" +
            COL_NOM+" TEXT, \n" +
            COL_VALEUREB+" DOUBLE, \n" +
            COL_VALEURR+" DOUBLE, \n" +
            COL_DATEACUIS+" TEXT, \n" +
            COL_DATEMES+" TEXT, \n" +
            COL_DUREE+" INTEGER \n" +
            ");";
    private DAOBase maBase;
    private SQLiteDatabase db;

    public MaterielDAO(Context context)
    {
        maBase = DAOBase.getInstance(context);
    }

    public void ouvrir()
    {
        db = maBase.getWritableDatabase();
    }

    public void fermer()
    {
        db.close();
    }

    @TargetApi(Build.VERSION_CODES.O)
    public long addMateriel(Materiel materiel) {
        ContentValues values = new ContentValues();
        values.put(COL_DATECREE, String.valueOf(materiel.getDateCree()));
        values.put(COL_NOM, materiel.getNom());
        values.put(COL_VALEUREB, materiel.getValeurEB());
        values.put(COL_VALEURR, materiel.getValeurR());
        values.put(COL_DATEACUIS, String.valueOf(materiel.getDateAcquis()));
        values.put(COL_DATEMES, String.valueOf(materiel.getDateMES()));
        values.put(COL_DUREE, materiel.getDureeReel());
        return db.insert(TABLE_NAME,null,values);
    }

    public int supMateriel(Materiel materiel) {
        String where = COL_ID_PK+" = ?";
        String[] whereArgs = {materiel.getId()+""};
        return db.delete(TABLE_NAME, where, whereArgs);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public Materiel getMateriel(int id) throws ParseException {
        Materiel mat = new Materiel();
        Cursor cur = db.rawQuery(
                "SELECT * " +
                    "FROM "+TABLE_NAME+" " +
                    "WHERE "+COL_ID_PK+"="+id+ " " +
                    "ORDER BY "+COL_DATECREE+" DESC", null
        );
        if (cur.moveToFirst()) {
            mat.setId(cur.getInt(cur.getColumnIndex(COL_ID_PK)));
            mat.setNom(cur.getString(cur.getColumnIndex(COL_NOM)));
            String cree = (cur.getString(cur.getColumnIndex(COL_DATECREE)));
            String acquis = (cur.getString(cur.getColumnIndex(COL_DATEACUIS)));
            String mes = (cur.getString(cur.getColumnIndex(COL_DATEMES)));
            Date dCree = null;
            Date dAcquis = null;
            Date dMES = null;
            dCree = new SimpleDateFormat("dd/MM/yyyy").parse(cree);
            dAcquis = new SimpleDateFormat("dd/MM/yyyy").parse(acquis);
            dMES = new SimpleDateFormat("dd/MM/yyyy").parse(mes);
            mat.setDateCree(dCree.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            mat.setDateAcquis(dAcquis.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            mat.setDateMES(dMES.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            mat.setValeurEB(cur.getDouble(cur.getColumnIndex(COL_VALEUREB)));
            mat.setValeurR(cur.getDouble(cur.getColumnIndex(COL_VALEURR)));
            mat.setDuree(cur.getInt(cur.getColumnIndex(COL_DUREE)));
            cur.close();
        }
        return mat;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Materiel> getMateriel() throws ParseException {
        List<Materiel> materiels = new ArrayList<Materiel>();
        Cursor cur =  db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex(COL_ID_PK));
            String nom = cur.getString(cur.getColumnIndex(COL_NOM));
            String cree = (cur.getString(cur.getColumnIndex(COL_DATECREE)));
            String acquis = (cur.getString(cur.getColumnIndex(COL_DATEACUIS)));
            String mes = (cur.getString(cur.getColumnIndex(COL_DATEMES)));
            Date dCree = null;
            Date dAcquis = null;
            Date dMES = null;
            dCree = new SimpleDateFormat("yyyy-MM-dd").parse(cree);
            dAcquis = new SimpleDateFormat("yyyy-MM-dd").parse(acquis);
            dMES = new SimpleDateFormat("yyyy-MM-dd").parse(mes);
            LocalDate dateCree = dCree.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate dateAcquis =  dAcquis.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate dateMES = dMES.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            double valeurEB = cur.getDouble(cur.getColumnIndex(COL_VALEUREB));
            double valeurR = cur.getDouble(cur.getColumnIndex(COL_VALEURR));
            int duree = cur.getInt(cur.getColumnIndex(COL_DUREE));
            Materiel mat = new Materiel(nom, valeurEB, valeurR, dateAcquis, dateMES, duree);
            mat.setId(id);
            mat.setDateCree(dateCree);
            materiels.add(mat);
        }
        return materiels;
    }

}
