package fr.ancelotow.amortization;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.amortization.entities.Lineaire;
import fr.ancelotow.amortization.entities.Materiel;
import fr.ancelotow.amortization.entities.Session;

public class LineairActivity extends AppCompatActivity {

    Button btnRetour;
    ListView lvAL;
    List<Lineaire> lineaires = new ArrayList<Lineaire>();
    DecimalFormat df = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Materiel materiel = Session.getSession().getMateriel();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lineair);
        setTitle("Linéaire : " + materiel.getNom());
        lvAL = (ListView) findViewById(R.id.lvAL);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            df = new DecimalFormat("0.000");
        }
        int nbAnnee = materiel.getDuree();
        double totalC = 0;
        double totalS = materiel.getBaseA();
        double firstAnnuite = 0;
        double lannuite = 0;
        for(int i = 0; i < nbAnnee; i++){
            Lineaire lin = new Lineaire();
            lin.setBase(materiel.getBaseA());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                lin.setAnnee(materiel.getDateMES().getYear() + i);
                if(materiel.getDateMES().getDayOfMonth() != 1){
                    if(i == 0){
                        double nbJoursUse = 30 - materiel.getDateMES().getDayOfMonth() +1 ;
                        int numMois = 12 - materiel.getDateMES().getMonthValue();
                        double taux = ((nbJoursUse + (30 * numMois)) / 360) * materiel.getTauxL();
                        lin.setAnnuite(taux * materiel.getBaseA());
                        firstAnnuite = lin.getAnnuite();

                    }else if(i + 1 == nbAnnee){
                        lin.setAnnuite(lannuite - firstAnnuite);
                    }else{
                        lin.setAnnuite(materiel.getBaseA() * materiel.getTauxL());
                        lannuite = lin.getAnnuite();
                    }
                } else{
                    lin.setAnnuite(materiel.getBaseA() * materiel.getTauxL());
                }
            }
            totalC = totalC + lin.getAnnuite();
            lin.setCumul(totalC);
            totalS = totalS - lin.getAnnuite();
            if(totalS < 0){
                lin.setValeurNC(0);
            }else{
                lin.setValeurNC(totalS);
            }
            lineaires.add(lin);
        }
        ItemLinAdaptateur item = new ItemLinAdaptateur();
        lvAL.setAdapter(item);
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(LineairActivity.this,
                        MenuActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        btnRetour.setOnClickListener(retour);
    }


    class ItemLinAdaptateur extends ArrayAdapter<Lineaire> {

        ItemLinAdaptateur(){
            super(
                    LineairActivity.this,
                    R.layout.item_lineaire,
                    R.id.tvAnnee,
                    lineaires
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvBase = (TextView) vItem.findViewById(R.id.tvBase);
            TextView tvAnnuite = (TextView) vItem.findViewById(R.id.tvAnnuite);
            TextView tvCumul = (TextView) vItem.findViewById(R.id.tvCumul);
            TextView tvValeurNC = (TextView) vItem.findViewById(R.id.tvValeurNC);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvBase.setText(String.valueOf(
                        df.format(lineaires.get(position).getBase())
                ));
                tvAnnuite.setText(String.valueOf(
                        df.format(lineaires.get(position).getAnnuite())
                ));
                tvCumul.setText(String.valueOf(
                        df.format(lineaires.get(position).getCumul())
                ));
                tvValeurNC.setText(String.valueOf(
                        df.format(lineaires.get(position).getValeurNC())
                ));
            }
            return vItem;
        }
    }

}
