package fr.ancelotow.amortization;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.amortization.entities.Degressif;
import fr.ancelotow.amortization.entities.Materiel;
import fr.ancelotow.amortization.entities.Session;

public class DegressifActivity extends AppCompatActivity {

    ListView lvAD;
    Button btnRetour;
    List<Degressif> degressifs = new ArrayList<Degressif>();
    DecimalFormat df = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Materiel materiel = Session.getSession().getMateriel();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_degressif);
        setTitle("Degressif : " + materiel.getNom());
        lvAD = (ListView) findViewById(R.id.lvAD);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            df =  new DecimalFormat("0.000");
        }
        int nbAnnee = materiel.getDureeReel();
        double totalC = 0;
        double totalS = materiel.getBaseA();
        double td = materiel.getTauxD();
        int annee = 0;
        while(nbAnnee > 0.00){
            Degressif deg = new Degressif();
            double tl = 1 / (double) nbAnnee;
            deg.setTl(tl);
            deg.setTd(td);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                deg.setAnnee(materiel.getDateMES().getYear() + annee);
            }
            annee = annee + 1;
            deg.setBase(totalS);
            if(td > tl){
                deg.setAnnuite(deg.getBase() * td);
            }else{
                deg.setAnnuite(deg.getBase() * tl);
            }
            nbAnnee = nbAnnee - 1;
            totalC = totalC + deg.getAnnuite();
            deg.setCumul(totalC);
            totalS = totalS - deg.getAnnuite();
            deg.setVnc(totalS);
            degressifs.add(deg);
        }
        ItemDegAdaptateur item = new ItemDegAdaptateur();
        lvAD.setAdapter(item);
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(DegressifActivity.this,
                        MenuActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        btnRetour.setOnClickListener(retour);
    }


    class ItemDegAdaptateur extends ArrayAdapter<Degressif> {

        ItemDegAdaptateur(){
            super(
                    DegressifActivity.this,
                    R.layout.item_degressif,
                    R.id.tvTL,
                    degressifs
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvTD = (TextView) vItem.findViewById(R.id.tvTD);
            TextView tvAnnee = (TextView) vItem.findViewById(R.id.tvAnnee);
            TextView tvBase = (TextView) vItem.findViewById(R.id.tvBase);
            TextView tvAnnuite = (TextView) vItem.findViewById(R.id.tvAnnuite);
            TextView tvCumul = (TextView) vItem.findViewById(R.id.tvCumul);
            TextView tvValeurNC = (TextView) vItem.findViewById(R.id.tvValeurNC);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvTD.setText(String.valueOf(
                        df.format(degressifs.get(position).getTd())
                ));
                tvAnnee.setText(String.valueOf(degressifs.get(position).getAnnee()));
                tvBase.setText(String.valueOf(
                        df.format(degressifs.get(position).getBase())
                ));
                tvAnnuite.setText(String.valueOf(
                        df.format(degressifs.get(position).getAnnuite())
                ));
                tvCumul.setText(String.valueOf(
                        df.format(degressifs.get(position).getCumul())
                ));
                tvValeurNC.setText(String.valueOf(
                        df.format(degressifs.get(position).getVnc())
                ));
            }
            return vItem;
        }
    }

}
