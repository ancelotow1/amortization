package fr.ancelotow.amortization;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.ParseException;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import fr.ancelotow.amortization.database.MaterielDAO;
import fr.ancelotow.amortization.entities.Materiel;
import fr.ancelotow.amortization.entities.Session;

public class MainActivity extends AppCompatActivity {

    EditText edNom;
    EditText edValeurEB;
    EditText edValeurR;
    EditText edDuree;
    TextView tvDateAcquis;
    TextView tvDateMES;
    Button btnDateAcquis;
    Button btnDateMES;
    Button btnValid;
    Button btnCancel;
    Button btnRetour;
    DatePickerDialog dialogDateAcquis;
    DatePickerDialog dialogDateMES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edNom = (EditText) findViewById(R.id.edNom);
        edValeurEB = (EditText) findViewById(R.id.edValeurEB);
        edValeurR = (EditText) findViewById(R.id.edValeurR);
        edDuree = (EditText) findViewById(R.id.edDuree);
        tvDateAcquis = (TextView) findViewById(R.id.tvDateAcquis);
        tvDateMES = (TextView) findViewById(R.id.tvDateMES);
        btnDateAcquis = (Button) findViewById(R.id.btnDateAcquis);
        btnDateMES = (Button) findViewById(R.id.btnDateMES);
        btnValid = (Button) findViewById(R.id.btnValid);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        btnDateAcquis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c;
                int mYear = 0;
                int mMonth = 0;
                int mDay = 0;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                }
                dialogDateAcquis = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                tvDateAcquis.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                dialogDateAcquis.show();
            }
        });
        btnDateMES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c;
                int mYear = 0;
                int mMonth = 0;
                int mDay = 0;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                }
                dialogDateMES = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                tvDateMES.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                dialogDateMES.show();
            }
        });
        View.OnClickListener annuler = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edNom.setText("");
                edValeurEB.setText("");
                edValeurR.setText("");
                edDuree.setText("");
                tvDateAcquis.setText("");
                tvDateMES.setText("");
                Toast.makeText(MainActivity.this, "Vous avez annulé cet ammortissement"
                        , Toast.LENGTH_LONG).show();
            }
        };
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ChoiceActivity.class);
                startActivity(i);
            }
        };
        View.OnClickListener valider = new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                String nom = edNom.getText().toString();
                double valeurEB = 0;
                double valeurR = 0;
                int duree = 0;
                Materiel materiel = null;
                String strDateAcquis = tvDateAcquis.getText().toString();
                LocalDate dateAcquis = null;
                String strDateMES = tvDateMES.getText().toString();
                LocalDate dateMES = null;
                try {
                    Date acquis = null;
                    Date mes = null;
                    valeurEB = Double.valueOf(edValeurEB.getText().toString());
                    valeurR = Double.valueOf(edValeurR.getText().toString());
                    duree = Integer.valueOf(edDuree.getText().toString());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        acquis = new SimpleDateFormat("dd/MM/yyyy").parse(strDateAcquis);
                        mes = new SimpleDateFormat("dd/MM/yyyy").parse(strDateMES);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            dateAcquis = acquis.toInstant().atZone(ZoneId.systemDefault()).
                                    toLocalDate();
                            dateMES = mes.toInstant().atZone(ZoneId.systemDefault()).
                                    toLocalDate();
                        }
                    }
                    if(valeurEB <= valeurR){
                        Toast.makeText(MainActivity.this,
                                "La valeur d'entrée brut doit être plus élevée que la valeur "+
                                        "résiduelle."
                                , Toast.LENGTH_LONG).show();
                    } else{
                        materiel = new Materiel(nom, valeurEB, valeurR,
                                dateAcquis, dateMES, duree);
                        MaterielDAO db = new MaterielDAO(MainActivity.this);
                        db.ouvrir();
                        long idMat = db.addMateriel(materiel);
                        db.fermer();
                        materiel.setId(idMat);
                        Session.ouvrir(materiel);
                        Toast.makeText(MainActivity.this,
                                "Ammortissement généré avec sucès."
                                , Toast.LENGTH_LONG).show();
                        Intent intentionEnvoyer = new Intent(MainActivity.this,
                                MenuActivity.class);
                        startActivity(intentionEnvoyer);
                    }
                } catch (NullPointerException |ParseException |
                        java.text.ParseException | NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,
                            "Tout les champs sont obligatoire"
                            , Toast.LENGTH_LONG).show();
                }
            }
        };
        btnCancel.setOnClickListener(annuler);
        btnValid.setOnClickListener(valider);
        btnRetour.setOnClickListener(retour);
    }

}
