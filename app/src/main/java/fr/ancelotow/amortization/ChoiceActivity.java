package fr.ancelotow.amortization;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChoiceActivity extends AppCompatActivity {

    Button btnConsultMat;
    Button btnCreateMat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);
        btnConsultMat = (Button) findViewById(R.id.btnConsultMat);
        btnCreateMat = (Button) findViewById(R.id.btnCreateMat);
        View.OnClickListener consulter = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChoiceActivity.this, ConsultActivity.class);
                startActivity(i);
            }
        };
        View.OnClickListener creer = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChoiceActivity.this, MainActivity.class);
                startActivity(i);
            }
        };
        btnConsultMat.setOnClickListener(consulter);
        btnCreateMat.setOnClickListener(creer);
    }

    @Override
    public void onBackPressed() {
        // do nothing.
    }

}
