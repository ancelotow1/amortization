package fr.ancelotow.amortization;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import fr.ancelotow.amortization.database.MaterielDAO;
import fr.ancelotow.amortization.entities.Session;

public class MenuActivity extends AppCompatActivity {

    Button btnFiche;
    Button btnDegressif;
    Button btnLineaire;
    Button btnRetour;
    Button btnSup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setTitle(Session.getSession().getMateriel().getNom());
        btnFiche = (Button) findViewById(R.id.btnFiche);
        btnDegressif = (Button) findViewById(R.id.btnDegressif);
        btnLineaire = (Button) findViewById(R.id.btnLineaire);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        btnSup = (Button) findViewById(R.id.btnSup);
        final AlertDialog.Builder alertQuit = new AlertDialog.Builder(this);
        alertQuit.setPositiveButton(R.string.btnOui, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                Session.fermer();
                Intent intentionEnvoyer = new Intent(MenuActivity.this,
                        ChoiceActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        alertQuit.setNegativeButton(R.string.btnNon, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertQuit.setMessage(R.string.tvQuit);
        final AlertDialog.Builder alertSup = new AlertDialog.Builder(this);
        alertSup.setPositiveButton(R.string.btnOui, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                MaterielDAO db = new MaterielDAO(MenuActivity.this);
                db.ouvrir();
                db.supMateriel(Session.getSession().getMateriel());
                db.fermer();
                Session.fermer();
                Intent intentionEnvoyer = new Intent(MenuActivity.this,
                        ConsultActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        alertSup.setNegativeButton(R.string.btnNon, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertSup.setMessage(R.string.tvSup);
        View.OnClickListener toFiche = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(MenuActivity.this,
                        FicheActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        View.OnClickListener toDegressif = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(MenuActivity.this,
                        DegressifActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        View.OnClickListener toLineaire = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(MenuActivity.this,
                        LineairActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertQuit.show();
            }
        };
        View.OnClickListener supprimer = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertSup.show();
            }
        };
        btnFiche.setOnClickListener(toFiche);
        btnDegressif.setOnClickListener(toDegressif);
        btnLineaire.setOnClickListener(toLineaire);
        btnRetour.setOnClickListener(retour);
        btnSup.setOnClickListener(supprimer);
    }

    @Override
    public void onBackPressed() {
        // do nothing.
    }

}
