package fr.ancelotow.amortization;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import fr.ancelotow.amortization.database.MaterielDAO;
import fr.ancelotow.amortization.entities.Materiel;
import fr.ancelotow.amortization.entities.Session;

public class ConsultActivity extends AppCompatActivity {

    Button btnRetour;
    ListView lvMat;
    protected List<Materiel> materiels = new ArrayList<Materiel>();
    DecimalFormat df = null;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            df =  new DecimalFormat("0.000");
        }
        lvMat = (ListView) findViewById(R.id.lvMat);
        btnRetour = (Button) findViewById(R.id.btnRetour);
        MaterielDAO db = new MaterielDAO(this);
        db.ouvrir();
        try {
            materiels = db.getMateriel();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ItemMatAdaptateur item = new ItemMatAdaptateur();
        lvMat.setAdapter(item);
        System.out.println("/////////////////// OUVRIR");
        lvMat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("/////////////////// rentréééééééééééé");
                Materiel materiel = materiels.get(i);
                Session.ouvrir(materiel);
                Toast.makeText(ConsultActivity.this,
                        "Consultation de l'ammortissement réussi."
                        , Toast.LENGTH_LONG).show();
                Intent intentionEnvoyer = new Intent(ConsultActivity.this,
                        MenuActivity.class);
                startActivity(intentionEnvoyer);
            }
        });
        View.OnClickListener retour = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentionEnvoyer = new Intent(ConsultActivity.this,
                        ChoiceActivity.class);
                startActivity(intentionEnvoyer);
            }
        };
        btnRetour.setOnClickListener(retour);
        db.fermer();
    }


    class ItemMatAdaptateur extends ArrayAdapter<Materiel> {

        ItemMatAdaptateur(){
            super(
                    ConsultActivity.this,
                    R.layout.item_materiel,
                    R.id.tvDateCreer,
                    materiels
            );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            View vItem = super.getView(position, convertView, parent);
            TextView tvNom = (TextView) vItem.findViewById(R.id.tvNom);
            TextView tvValeurEB = (TextView) vItem.findViewById(R.id.tvValeurEB);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvNom.setText(materiels.get(position).getNom());
                tvValeurEB.setText(String.valueOf(
                        df.format(materiels.get(position).getValeurEB())
                ));
            }
            return vItem;
        }
    }
}
