package fr.ancelotow.amortization.entities;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Materiel {

    private long id;
    private String nom;
    private double valeurEB;
    private double valeurR;
    private double baseA;
    private LocalDate dateCree;
    private LocalDate dateAcquis;
    private LocalDate dateMES;
    private LocalDate dateDebAD;
    private LocalDate dateDebAL;
    private int dureeReel;
    private int duree;
    private double tauxL;
    private double coeff;
    private double tauxD;

    public Materiel(){
        super();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Materiel(String nom, double valeurEB, double valeurR, LocalDate dateAquis,
                    LocalDate dateMES, int duree){
        super();
        this.nom = nom;
        this.valeurEB = valeurEB;
        this.valeurR = valeurR;
        this.baseA = this.valeurEB - this.valeurR;
        this.dateCree = LocalDate.now();
        this.dateAcquis = dateAquis;
        this.dateMES = dateMES;
        this.dureeReel = duree;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.dateDebAD = LocalDate.of(this.dateMES.getYear(),
                                            this.dateMES.getMonthValue(),
                                            1);
        }
        this.dateDebAL = this.dateMES;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if(this.dateMES.getMonthValue() != 1 && this.dateMES.getDayOfMonth() != 1 ){
                this.duree = duree + 1;
            }else{
                this.duree = duree;
            }
        }
        this.tauxL = 1 / (double) duree;
        double leCoeff;
        if(duree == 3 || duree == 4){
            leCoeff = 1.25;
        }
        else if(duree == 5 || duree == 6){
            leCoeff = 1.75;
        }
        else{
            leCoeff = 2.25;
        }
        this.coeff = leCoeff;
        this.tauxD = this.tauxL * this.coeff;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getValeurEB() {
        return valeurEB;
    }

    public void setValeurEB(double valeurEB) {
        this.valeurEB = valeurEB;
    }

    public double getValeurR() {
        return valeurR;
    }

    public void setValeurR(double valeurR) {
        this.valeurR = valeurR;
    }

    public double getBaseA() {
        return baseA;
    }

    public void setBaseA(double baseA) {
        this.baseA = baseA;
    }

    public LocalDate getDateAcquis() {
        return dateAcquis;
    }

    public void setDateAcquis(LocalDate dateAcquis) {
        this.dateAcquis = dateAcquis;
    }

    public LocalDate getDateMES() {
        return dateMES;
    }

    public void setDateMES(LocalDate dateMES) {
        this.dateMES = dateMES;
    }

    public LocalDate getDateDebAD() {
        return dateDebAD;
    }

    public void setDateDebAD(LocalDate dateDebAD) {
        this.dateDebAD = dateDebAD;
    }

    public LocalDate getDateDebAL() {
        return dateDebAL;
    }

    public void setDateDebAL(LocalDate dateDebAL) {
        this.dateDebAL = dateDebAL;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public double getTauxL() {
        return tauxL;
    }

    public void setTauxL(double tauxL) {
        this.tauxL = tauxL;
    }

    public double getCoeff() {
        return coeff;
    }

    public void setCoeff(double coeff) {
        this.coeff = coeff;
    }

    public double getTauxD() {
        return tauxD;
    }

    public void setTauxD(double tauxD) {
        this.tauxD = tauxD;
    }

    public int getDureeReel() {
        return dureeReel;
    }

    public void setDureeReel(int dureeReel) {
        this.dureeReel = dureeReel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDateCree() {
        return dateCree;
    }

    public void setDateCree(LocalDate dateCree) {
        this.dateCree = dateCree;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public String toString(){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("d/MM/uuuu");
        return this.dateCree.format(df);
    }

}
