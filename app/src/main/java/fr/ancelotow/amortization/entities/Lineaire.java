package fr.ancelotow.amortization.entities;

public class Lineaire {

    private int annee;
    private double base;
    private double annuite;
    private double cumul;
    private double valeurNC;

    public Lineaire(){
        super();
    }

    public Lineaire(int annee, double base, double annuite, double cumul, double valeurNC) {
        this.annee = annee;
        this.base = base;
        this.annuite = annuite;
        this.cumul = cumul;
        this.valeurNC = valeurNC;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAnnuite() {
        return annuite;
    }

    public void setAnnuite(double annuite) {
        this.annuite = annuite;
    }

    public double getCumul() {
        return cumul;
    }

    public void setCumul(double cumul) {
        this.cumul = cumul;
    }

    public double getValeurNC() {
        return valeurNC;
    }

    public void setValeurNC(double valeurNC) {
        this.valeurNC = valeurNC;
    }

    @Override
    public String toString(){
        return String.valueOf(annee);
    }

}
