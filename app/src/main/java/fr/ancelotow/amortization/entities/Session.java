package fr.ancelotow.amortization.entities;

import fr.ancelotow.amortization.database.MaterielDAO;

public class Session {

    private static Session session = null;
    private Materiel materiel;
    private MaterielDAO db;

    private Session(Materiel materiel){
        super();
        this.materiel = materiel;
    }

    public static void ouvrir(Materiel materiel){
        if(session == null){
            session = new Session(materiel);
        }
    }

    public static void fermer(){
        session = null;
    }

    public static Session getSession(){
        return session;
    }

    public Materiel getMateriel(){
        return materiel;
    }

}
