package fr.ancelotow.amortization.entities;

import android.icu.text.DecimalFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;

public class Degressif {

    private double tl;
    private double td;
    private int annee;
    private double base;
    private double annuite;
    private double cumul;
    private double vnc;

    public Degressif(){
        super();
    }

    public Degressif(double tl, double td, int annee, double base, double annuite, double cumul, double vnc) {
        this.tl = tl;
        this.td = td;
        this.annee = annee;
        this.base = base;
        this.annuite = annuite;
        this.cumul = cumul;
        this.vnc = vnc;
    }

    public double getTl() {
        return tl;
    }

    public void setTl(double tl) {
        this.tl = tl;
    }

    public double getTd() {
        return td;
    }

    public void setTd(double td) {
        this.td = td;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAnnuite() {
        return annuite;
    }

    public void setAnnuite(double annuite) {
        this.annuite = annuite;
    }

    public double getCumul() {
        return cumul;
    }

    public void setCumul(double cumul) {
        this.cumul = cumul;
    }

    public double getVnc() {
        return vnc;
    }

    public void setVnc(double vnc) {
        this.vnc = vnc;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public String toString() {
        DecimalFormat df = null;
        df =  new DecimalFormat("0.000");
        return String.valueOf(df.format(tl));
    }
}
